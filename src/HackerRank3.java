import java.util.*;

public class HackerRank3 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int integer = scanner.nextInt();
        double doubleValue = scanner.nextDouble();
        String stringValue = scanner.next();
        stringValue+=scanner.nextLine();
        System.out.println("String: " + stringValue + "\n" + "Double:" + doubleValue + "\n" + "Int: " + integer);
    }
}
